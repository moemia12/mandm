import { images } from '../profileimages/images'

const dataAndis = [{
    "name": "Eliana Calote",
    "details": [
        {
            "and_title":"Music lover",
            "level":"1",
            "job_title":"Associate PD",
            "profilePic": images.eliana
        }
    ]   
},
{
    "name": "Blessin Opara",
    "details": [
        {
            "and_title":"Content Creator",
            "level":"2.1",
            "job_title":"Associate PD",
            "profilePic": images.blessin
        }
    ]   
},
{
    "name": "Mo Miah",
    "details": [
        {
            "and_title":"Nebula",
            "level":"2.1",
            "job_title":"Associate PD",
            "profilePic": images.momiah
        }
    ]   
},
{
    "name": "Andrina Ballantyne",
    "details": [
        {
            "and_title":"Music Lover",
            "level":"6",
            "job_title":"Squad Lead",
            "profilePic": images.andrina
        }
    ]   
},
{
    "name": "Rupa Shikotra",
    "details": [
        {
            "and_title":"Professional Maverick",
            "level":"3",
            "job_title":"UI/UX Designer",
            "profilePic": images.rupa
        }
    ]   
},
{
    "name": "Alex Eftyhiou",
    "details": [
        {
            "and_title":"Musical Adventurer",
            "level":"4.3",
            "job_title":"UX Designer",
            "profilePic": images.alex
        }
    ]   
},
{
    "name": "Peter Nunn",
    "details": [
        {
            "and_title":"Avid Rambler",
            "level":"3",
            "job_title":"UX Designer",
            "profilePic": images.peter
        }
    ]   
},
{
    "name": "George Taylor",
    "details": [
        {
            "and_title":"Football Fanatic",
            "level":"3.1",
            "job_title":"Product Developer",
            "profilePic": ""
        }
    ]   
},
{
    "name": "Andy Davison",
    "details": [
        {
            "and_title":"Music Lover",
            "level":"5.1",
            "job_title":"Product Developer",
            "profilePic": ""
        }
    ]   
},
{
    "name": "Jana Kunft",
    "details": [
        {
            "and_title":"Pumpkin Plunderer",
            "level":"5.3",
            "job_title":"Product Developer",
            "profilePic": ""
        }
    ]   
},
{
    "name": "Megan Wharton",
    "details": [
        {
            "and_title":"Bibliophile",
            "level":"5.1",
            "job_title":"Product Analyst",
            "profilePic": ""
        }
    ]   
},
{
    "name": "Aditya Modali",
    "details": [
        {
            "and_title":"Food Enthusiast",
            "level":"6",
            "job_title":"Squad Lead",
            "profilePic": ""
        }
    ]   
},
{
    "name": "Niladree Bhattacharjee",
    "details": [
        {
            "and_title":"Adventure Sports Enthusiast",
            "level":"5.3",
            "job_title":"Product Developer",
            "profilePic": ""
        }
    ]   
},
{
    "name": "Alexandra Boadu",
    "details": [
        {
            "and_title":"Creative Savvy",
            "level":"2.1",
            "job_title":"Associate UX Designer",
            "profilePic": ""
        }
    ]   
},
{
    "name": "Ebenezer Olatunji",
    "details": [
        {
            "and_title":"Music Producer",
            "level":"3.2",
            "job_title":"Product Analyst",
            "profilePic": ""
        }
    ]   
},
{
    "name": "Giby Thomas",
    "details": [
        {
            "and_title":"Fruit Gardener",
            "level":"5.1",
            "job_title":"Product Developer",
            "profilePic": ""
        }
    ]   
},
{
    "name": "Parum Cheema",
    "details": [
        {
            "and_title":"Foodie Traveller",
            "level":"4.1",
            "job_title":"Product Analyst",
            "profilePic": ""
        }
    ]   
},
{
    "name": "Tanwen Rolph",
    "details": [
        {
            "and_title":"Cheese Addict",
            "level":"5.1",
            "job_title":"Product Analyst",
            "profilePic": ""
        }
    ]   
},
{
    "name": "Viktoria Idakieva",
    "details": [
        {
            "and_title":"Artistic Gamer",
            "level":"3.1",
            "job_title":"Product Developer",
            "profilePic": ""
        }
    ]   
}
]

export default dataAndis
