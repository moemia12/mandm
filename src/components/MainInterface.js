import React, { useState } from 'react'
import Search from '../components/InputFields/Search'
import ProfileCards from './Profile/ProfileCards'
import JobTitle from '../components/InputFields/JobTitle';
import Level from './InputFields/Level';

const MainInterface = () => {
  const [searchResult, setSearchResult] = useState([]);

    return (
      <div className='Interface-Container'>
        <div>
        <Search setSearchResult={setSearchResult}/>
        <JobTitle/>
        <Level/>
        </div>
        
        <ProfileCards searchResult={searchResult}/>
        
        
      </div>
    )
}

export default MainInterface