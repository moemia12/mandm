import React from "react";

const ProfileCards = ({searchResult}) => {

    const chooseImage = (andi) =>{
        if(!andi.details[0].profilePic){
            return <div className="profile-pic"><i class='fas fa-user-alt'></i></div>
        }
        return <img className="profile-pic" src={andi.details[0].profilePic}/>
    }

  const dataItems = searchResult.map((andi) => {
    return (
      <div className="profile-card">
         {chooseImage(andi)}
        <div className="bio-container">
          <div className="user-info">
            <div className="name">{andi.name}</div>
            <div className="job-title">{andi.details[0].job_title}</div>
          </div>
          <div className="profile-type">
            <div className="level">{andi.details[0].level}</div>
            <div className="mentor-type">m</div>
          </div>
        </div>
      </div>
    );
  });

  return <div className="mappedData">{dataItems}</div>;
};

export default ProfileCards;
