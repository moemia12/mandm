import React from "react";

const JobTitle = () => {
  return (
    <div className="jobtitle-container">
        <i class="fa fa-caret-down"></i>
      <div className="jobTitle-alt">Job Title</div>
    </div>
  );
};

export default JobTitle;