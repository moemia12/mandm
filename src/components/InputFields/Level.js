import React from "react";


const Level = () => {
  return (
    <div className="level-container">
         <i class="fa fa-caret-down"></i>
      <div className="level-alt">Level</div>
    </div>
  );
};

export default Level;