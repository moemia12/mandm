import React, { useState } from "react";
import dataAndis from "../../data/andis-data";
import searchicon from "../../searchicon.svg";

const Search = ({setSearchResult}) => {
  const [searchString, setSearchString] = useState("");

  const onInputChange = (e) => {
    const input = e.target.value;
    console.log(input)

    setSearchString(input);
    searchFunction();
    console.log("onInputChange")
  };
  
  const searchFunction = () => {
    console.log("searchFunction")
    const result = dataAndis.filter((value) => {
      return (
        value.name.match(new RegExp(searchString, "i"))
      );
    });

    setSearchResult(result);
  };

  return (
    <div className="input-container">
      <img src={searchicon} alt="icon" className="search-icon" />
      <input
        className="input-alt"
        type="search"
        placeholder="Search ANDis"
        onChange={onInputChange}
      ></input>
    </div>
  );
};

export default Search;
