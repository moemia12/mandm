import React from "react";
import mandmlogo from '../mandm-logo.svg'

const Header = () => {
    return(
        <div className="header">
            <img src={mandmlogo} alt="searchlogo"/>
        </div>
    )
}

export default Header